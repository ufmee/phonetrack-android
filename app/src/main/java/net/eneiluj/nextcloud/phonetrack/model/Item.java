package net.eneiluj.nextcloud.phonetrack.model;

public interface Item {
    boolean isSection();
}
